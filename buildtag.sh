#!/bin/bash

# Generates: 
#   - output/buildtag -- This file can be sourced in shell scripts to
#       provide the BUILDTAG variable. It also provides the build # 
#       for the assets zip.
#
# Responsible for determining our current release version and buildtag.
#
# For master branch this is determined using the peach-web.properties file
# and git describe to find the last tagged build and increment by one.
#
# For non-master branch build the version is always 0.0.0
#  

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

source $DIR/har-recorder.properties

if [[ "$CI_COMMIT_BRANCH" == "main" ]]; then

    last=$(git describe --match ${TAG_PREFIX}${RELEASE_VERSION}.* --abbrev=0 | cut -f3 -d.)
    next=$((last+1))

    echo BUILDTAG=${RELEASE_VERSION}.${next}
    echo BUILDTAG=${RELEASE_VERSION}.${next} > $DIR/buildtag
    export BUILDTAG=${RELEASE_VERSION}.${next}
else
    echo BUILDTAG=0.0.0
    echo BUILDTAG=0.0.0 > $DIR/buildtag
    export BUILDTAG=0.0.0
fi

# end

